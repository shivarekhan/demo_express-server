const express = require('express');
const path = require('path');

const port = process.env.PORT || 3000

const app = express();

const pathForHtml = path.join(__dirname, "/static/response.html");


app.get('/', function (req, res) {
    console.log(``)
    res.sendFile(pathForHtml);
});


app.listen(port);